﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private List<WaveConfig> waveConfigs;
    [SerializeField] private bool looping;

    private const int StartingWave = 0;

    // Start is called before the first frame update
    private IEnumerator Start()
    {
        do
        {
            yield return StartCoroutine(SpawnAllWaves());
        } while (looping);
    }

    private IEnumerator SpawnAllWaves()
    {
        for (var waveIndex = StartingWave; waveIndex < waveConfigs.Count; waveIndex++)
        {
            var currentWave = waveConfigs[waveIndex];

            yield return StartCoroutine(SpawnAllEnemiesInWave(currentWave));
        }
    }

    private IEnumerator SpawnAllEnemiesInWave(WaveConfig waveConfig)
    {
        for (var enemyCount = 0; enemyCount < waveConfig.NumberOfEnemies; enemyCount++)
        {
            var newEnemy = Instantiate(
                waveConfig.EnemyPrefab,
                waveConfig.GetWaypoints()[0].transform.position,
                Quaternion.AngleAxis(90, Vector3.back));
            newEnemy.GetComponent<EnnemyPathing>().WaveConfig = waveConfig;

            yield return new WaitForSeconds(waveConfig.TimeBetweenSpawns);
        }
    }
}