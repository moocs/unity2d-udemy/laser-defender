﻿using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Player")] [SerializeField] private float moveSpeed = 10f;
    [SerializeField] private float generalPadding = 1f;
    [SerializeField] private float horizontalLimit = 0.75f;
    [SerializeField] private int health = 500;

    [Header("Projectiles")] [SerializeField]
    private GameObject laserPrefab;

    [SerializeField] private float projectileSpeed = 10f;
    [SerializeField] private float projectileFiringPeriod = 0.05f;
    [SerializeField] private GameObject destroyParticlesEffect;

    [Header("SoundEffects")] [SerializeField]
    private AudioClip laserSound;

    [SerializeField] [Range(0, 1)] private float laserSoundVolume = .2f;

    [SerializeField] private AudioClip explosionSound;
    [SerializeField] [Range(0, 1)] private float deathSoundVolume = 1f;

    private float _yMin, _yMax, _xMin, _xMax;
    private Camera _cameraMain;
    private SceneLoader _sceneLoader;

    // Start is called before the first frame update
    private void Start()
    {
        _sceneLoader = FindObjectOfType<SceneLoader>();
        _cameraMain = Camera.main;
        Debug.Assert(_cameraMain != null, "Camera.main != null");
        SetUpMoveBoundaries();
    }

    private void SetUpMoveBoundaries()
    {
        var gameCamera = Camera.main;

        if (gameCamera == null) return;
        _yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + generalPadding;
        _yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - generalPadding;
        _xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + generalPadding;
        _xMax = gameCamera.ViewportToWorldPoint(new Vector3(horizontalLimit, 0, 0)).x - generalPadding;
    }

    // Update is called once per frame
    private void Update()
    {
        Move();
        Fire();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        if (!damageDealer) return;

        ProcessHit(damageDealer);
    }

    private void ProcessHit(DamageDealer damageDealer)
    {
        health -= damageDealer.Damage;
        damageDealer.Hit();
        if (health <= 0) Die();
    }

    private void Die()
    {
        Destroy(gameObject);
        var position = transform.position;
        GameObject explosion = Instantiate(destroyParticlesEffect, position, Quaternion.identity);
        Destroy(explosion, 1f);

        AudioSource.PlayClipAtPoint(explosionSound, _cameraMain.transform.position, deathSoundVolume);
        _sceneLoader.LoadGameOver(2f);
    }


    private void Move()
    {
        var transform1 = transform;
        var position = transform1.position;

        // Deltas
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;

        // New Positions
        var newYPos = Mathf.Clamp(position.y + deltaY, _yMin, _yMax);
        var newXPos = Mathf.Clamp(position.x + deltaX, _xMin, _xMax);

        // Updating position
        position = new Vector2(newXPos, newYPos);
        transform1.position = position;
    }

    private void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            StartCoroutine(FireContinuously());
        }
    }

    private IEnumerator FireContinuously()
    {
        while (Input.GetButton("Fire1"))
        {
            var laser = Instantiate(
                laserPrefab,
                transform.position,
                Quaternion.AngleAxis(90, Vector3.back));
            laser.GetComponent<Rigidbody2D>().velocity = new Vector2(projectileSpeed, 0);
            AudioSource.PlayClipAtPoint(laserSound, _cameraMain.transform.position, laserSoundVolume);

            yield return new WaitForSeconds(projectileFiringPeriod);
        }
    }

    public int Health => health;
}