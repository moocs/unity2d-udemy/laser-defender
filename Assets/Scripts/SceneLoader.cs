﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadGameScene()
    {
        var gameSession = FindObjectOfType<GameSession>();
        if (gameSession) gameSession.ResetGame();
        SceneManager.LoadScene("1. Main Screen");
    }

    public void LoadGameOver(float delaySeconds)
    {
        StartCoroutine(GameOverWithDelay(delaySeconds));
    }

    private static IEnumerator GameOverWithDelay(float delaySeconds)
    {
        yield return new WaitForSeconds(delaySeconds);

        SceneManager.LoadScene("2. Game Over");
    }

    public void LoadInitialScene()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}