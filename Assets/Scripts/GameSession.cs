﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameSession : MonoBehaviour
{
    private int _score;

    private void Awake()
    {
        SetUpSingleton();
    }
    
    private void SetUpSingleton()
    {
        // Implementing Singleton
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            // If there are already existing gameStatus, destroy yourself as you're not the first one
            GameObject o;
            (o = gameObject).SetActive(false);
            Destroy(o);
        }
        else
        {
            // Otherwise, you're the first : you can exist and you should not get destroyed at the end of the Scene
            DontDestroyOnLoad(gameObject);
        }
    }


    public void AddToScore(int scoreToAdd)
    {
        _score += scoreToAdd;
    }

    public int Score
    {
        get => _score;
        set => _score = value;
    }
    
    public void ResetGame()
    {
        Destroy(gameObject);
    }
}