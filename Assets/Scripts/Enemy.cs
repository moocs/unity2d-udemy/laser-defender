﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("Enemy")] [SerializeField] private float health = 100;
    [SerializeField] private GameObject destroyParticlesEffect;
    [SerializeField] private int destroyScore = 100;

    [Header("Shots")] [SerializeField] private float minTimeBetweenShots = 0.2f;
    [SerializeField] private float maxTimeBetweenShots = 3f;
    [SerializeField] private float projectileSpeed = 10f;
    [SerializeField] private GameObject laserPrefab;

    [Header("SoundEffects")] [SerializeField]
    private AudioClip laserSound;

    [SerializeField] [Range(0, 1)] private float laserSoundVolume = 1f;
    [SerializeField] private AudioClip explosionSound;
    [SerializeField] [Range(0, 1)] private float deathSoundVolume = .5f;

    private float _shotCounter;
    private Camera _cameraMain;

    // Start is called before the first frame update
    private void Start()
    {
        _cameraMain = Camera.main;
        Debug.Assert(_cameraMain != null);
        ShotCounterInit();
    }

    private void ShotCounterInit()
    {
        _shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
    }

    // Update is called once per frame
    private void Update()
    {
        CountDownAndShoot();
    }

    private void CountDownAndShoot()
    {
        _shotCounter -= Time.deltaTime;
        if (!(_shotCounter <= 0)) return;
        Fire();
        ShotCounterInit();
    }

    private void Fire()
    {
        GameObject laser = Instantiate(
            laserPrefab,
            transform.position,
            Quaternion.AngleAxis(-90, Vector3.back));
        laser.GetComponent<Rigidbody2D>().velocity = new Vector2(-1 * projectileSpeed, 0);

        AudioSource.PlayClipAtPoint(laserSound, _cameraMain.transform.position, laserSoundVolume);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        if (!damageDealer) return;
        ProcessHit(damageDealer);
    }

    private void ProcessHit(DamageDealer damageDealer)
    {
        health -= damageDealer.Damage;
        damageDealer.Hit();
        if (health <= 0) Die();
    }

    private void Die()
    {
        Destroy(gameObject);

        var position = transform.position;
        GameObject explosion = Instantiate(destroyParticlesEffect, position, Quaternion.identity);
        Destroy(explosion, 1f);
        
        AudioSource.PlayClipAtPoint(explosionSound, _cameraMain.transform.position, deathSoundVolume);
        
        FindObjectOfType<GameSession>().AddToScore(destroyScore);
    }
}