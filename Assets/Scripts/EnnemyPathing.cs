﻿using System.Collections.Generic;
using UnityEngine;

public class EnnemyPathing : MonoBehaviour
{
    private WaveConfig _waveConfig;
    private float _moveSpeed = 5f;
    private int _waypointIndex;
    private List<Transform> _waypoints;


    // Update is called once per frame
    private void Update()
    {
        Move();
    }

    public WaveConfig WaveConfig
    {
        get => _waveConfig;
        set
        {
            _waveConfig = value;
            _moveSpeed = _waveConfig.MoveSpeed;
            _waypoints = _waveConfig.GetWaypoints();
            transform.position = _waypoints[_waypointIndex].transform.position;
        }
    }

    private void Move()
    {
        if (_waypointIndex < _waypoints.Count)
        {
            var targetPosition = _waypoints[_waypointIndex].transform.position;
            var movementThisFrame = _moveSpeed * Time.deltaTime;

            transform.position = Vector2.MoveTowards(transform.position, targetPosition, movementThisFrame);

            if (transform.position == targetPosition)
            {
                _waypointIndex++;
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }
}