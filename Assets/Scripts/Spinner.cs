﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{

    [SerializeField] private float rotatingSpeed = 100f;
    
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.back, rotatingSpeed * Time.deltaTime);
    }
}
